<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of 8a3e4bcea4f5d1439cbd34c7f6e66313738b42Test
 *
 * @author abhishek
 */
class 8a3e4bcea4f5d1439cbd34c7f6e66313738b42Test extends PHPUnit_Framework_TestCase {

  /**
   * @var \RemoteWebDriver
   */
  protected $webDriver;

  public function setUp() {
    $capabilities = array(\WebDriverCapabilityType::BROWSER_NAME => 'firefox');
    $this->webDriver = RemoteWebDriver::create('http://localhost:4444/wd/hub', $capabilities);
  }

  public function tearDown() {
    $this->webDriver->close();
  }

  protected $url = 'http://www.netbeans.org/';

  public function testSimple() {
    $this->webDriver->get($this->url);
    // checking that page title contains word 'NetBeans'
    $this->assertContains('NetBeans', $this->webDriver->getTitle());
  }

}
